# Fundamentos de programación
   ## Actividad 7 Creando algoritmos con soluciones secuenciales

### ejercicio 1:
     Escritura un algoritmo que le permita al usuario seleccionar qué tipo de conversión quiere realizar y recibir el valor que desea convertir. El sistema debe hacer el cálculo y mostrar en pantalla el resultado de la conversión


 ![picture alt](image/diagrama1.png "diagrama 1")

### ejercicio 2:
      Cree un algortimo que lea la cantidad de estudiantes del curso "Fundamentos de programacion y algorítmica básica", "Introducción a la ingenieria de software", "Introducción a la  modalidad virtual" y "Habilidades de la comunicación".
      Una vez  capturados estos valores por pantalla el algortimo debe hacer el calculo del promedio de estudiantes en los 4 cursos 

    Se debe tener en cuenta que si alguno de estos cursos no abrió, es decir tiene (0) estudiantes, no se incluirá el curso en el promedio de estudiantes en cursos.

### ejercicio 3:
    Cree un algoritmo que dado un número de tres cifras, haga los siguiente :
- Si es par: invierta el número
- Si es impar: muestre en pantalla la suma de los extremos e invierta el número
 - calcule el cuadrado del número ingresado y el cuadrado de los extremos


### ejercicio 4:
     Cree un algoritmo que dados  dos números enteros y un símbolo(+,-,* y /), dé el cálculo correspondiente de acuerdo con el símbolo ingresado y muestre en pantalla el resultado de la operación. Esta solución debe implementar el operador "CASE" 